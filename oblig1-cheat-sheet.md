# Cheat sheet for Oblig1

The task in Oblig1 is quite similar to what we have been studying at the lectures, with the only difference that there are more language constructs to specify.

## Recall the BIPL language from the slides

Link to the slides: https://mitt.uib.no/courses/45696/files/folder/lecture-slides?preview=5667496

### Haskell specification of BIPL's abstract syntax

```haskell
module Main where

-- statements
data Stmt
  = Skip
  | Assign String Expr
  | Seq Stmt Stmt
  | If Expr Stmt Stmt
  | While Expr Stmt
  deriving Show

-- expressions
data Expr
  = IntConst Int
  | BoolConst Bool
  | Var String
  | Unary UOp Expr
  | Binary BOp Expr Expr
  deriving Show

-- unary operators
data UOp
  = Negate
  | Not
  deriving Show
  
-- binary operators
data BOp
  = Add
  | Sub
  | Mul
  | Lt
  | Leq
  | Eq
  | Geq
  | Gt
  | And
  | Or
  deriving Show
```
	
### Zephyr ASDL specification of BIPL's abstract syntax

```
Program = { statements: [Statement] }

Statement
	= AssignmentStatement
	| IfStatement
	| WhileStatement
	| BlockStatement

AssignmentStatement = { variable: string, value: Expression }

IfStatement = { condition: Expression, trueBranch: Statement, falseBranch: Statement }

WhileStatement =  { condition: Expression, body: Statement }

BlockStatement = { stmts: [Statement] }

Expression
	= IntegerLiteral | BooleanLiteral | StringLiteral
	| VariableReference { name: string }
	| BinaryExpression { left: Expression, op: BOp, right: Expression }
	| UnaryExpression { op: UOp, right: Expression }

BOp
	= Add
	| Sub
	| Mul
	| Lt
	| Leq
	| Eq
	| Geq
	| Gt
	| And
	| Or
```

### A grammar for BIPL's concrete syntax

```
Statement
	: AssignmentStatement
	| IfStatement
	| WhileStatement
	| BlockStatement
;

AssignmentStatement :
	ident '=' Expression
;

IfStatement :
	'if' '(' Expression ')' '{'
		Statement
	'}'
	{
		'else' '{'
			Statement
		'}'
	}?
;

WhileStatement :
	'while' '(' Expression ')' '{'
		Statement
	'}'
;

BlockStatement :
	'{' { Statement }* '}'
;

Expression
	: PrimitiveExpression
	| Expression BinaryOp Expression
	| UnaryOp Expression 
	| '(' Expression ')'
;

PrimitiveExpression
	: IntegerLiteral
	| 'true'
	| 'false'
	| StringLiteral
	| ident
;

BinaryOp : '+' | '-' | '*' | '/' | '>' | '<' | '>=' | '<=' | '==' | '=!='| '&&' | '||'
;

UnaryOp : '!' | '-'
;

IntegerLiteral : // we do not define it explicitly, but rather take it for granted for now
;

StringLiteral : // we do not define it explicitly, but rather take it for granted for now
;

```

## More links

* Group session's explanations about abstract syntax: https://docs.google.com/presentation/d/19CCIte-XRXvCxZ80Dw9FEF3CX6MybOkBAu8pAMZbmo8/edit?usp=sharing

* Lecture slides: https://mitt.uib.no/courses/45696/files/folder/lecture-slides?preview=5678149 (slides: 10, 11, 12, 13, 14, 15, 16, 17)

* An example of Zephyr ASDL specification (from the lecture on 30.1.2024): https://mitt.uib.no/courses/45696/files?preview=5681878
