# INF222 V24 - Obligatory Assignment 2 (deadline: 10.4.2024 at 23:59)

## Context

In this assignment, you are asked to implement a **parser**, a **static analyzer (type checker)**, and an **interpreter (evaluator)** for a simple language which we will call _PhysicsLang_ (_"PL"_).

Let's have a look at an example program written in PL:
```js
let a = 10[kg] + 100[g];
let b = 25[m] + 5[km] + (10[m] + 2[km]);
let c = 3[km];
let d = b + c;
let e = b + 3[m];
let f = 2[h] + 15[min] + 5[s] - 2[s] - 1[h];
let g = 2[m/s] + 10[km/min] + 42[m/h];
let blabla = b / f + g + 1[km/s];
let badexample = 1[kg] + 2[m/s]; // mismatching units: cannot add a mass unit to a velocity unit
```

After interpreting such a program, the interpreter should output the values of each assigned variable, as demonstrated below:
```
a : 10100 g
b : 7035 m
c : 3000 m
d : 10035 m
e : 7038 m
f : 4503 s
g : 168.6816 m/s
blabla : 1170.2439 m/s
```

### Some remarks
- A program written in this language is a sequence of assignment statements.
- An assignment statement binds a variable to a value represented by an arithmetic expression.
- Arithmetic expressions are like normal arithmetic expressions, but each integer literal additionally is accompanied by a physical unit.
- Physical units should match, i.e., one can add length to length, but one shouldn't be allowed to add mass to velocity, for example.
- Formally, this language only has one datatype (`integer`), but because of the support of various physical units, one can tell that each physical unit gives rise to a type in this language (e.g., kilograms, meters, minutes, meters-per-hour, and so on).
- Please note that some units have been converted to "basic" units. More on this below.

### What should you do?
- We give you a skeleton of the solution, and you should understand it and fill in the missing parts of code.
- This document explains the idea of the solution. Please also read comments in the code itself.
- The metalanguage (i.e., the language in which you implement the parser, the static analyzer, and the interpreter) is **TypeScript**.

## Why this task and why TypeScript?
- This Obligatory Assignment is supposed to deliver on the INF222 learning outcomes, which state that a student is able to implement a parser, a static analyzer, and an interpreter/evaluator for a small language with types.
- Being able to grasp the main concepts of a trending programming language (in this case, TypeScript) is also a part of the learning outcomes for this course.
- TypeScript is one of the most popular programming languages (https://octoverse.github.com/2022/top-programming-languages - scroll for the diagram), and it is used in an overwhelming majority of various projects, including frontend and backend development. Having some experience with TypeScript will be more and more of a requirement in job search.
- This assignment is also supposed to give you experience in understanding an existing codebase in a somewhat less familiar language (i.e., TypeScript), as well as experience in modifying a codebase according to _very precise_ requirement specifications (in the style of technical standards documents).

## Details

### Which physical units are supported in this language?

We support only a very limited set of units:

|[physical quantity](https://en.wikipedia.org/wiki/List_of_physical_quantities)|physical units|
|---|---|
|mass|kilograms (`kg`), grams (`g`)|
|length|kilometers (`km`), meters (`m`)|
|time|hours (`h`), minutes (`min`), seconds (`s`)|
|velocity|`km/h`, `km/min`, `km/s`, `m/h`, `m/min`, `m/s`|

### What do you need to implement? 

* A **parser**: it will take as an input the textual representation of a program written in _PhysicsLang_, and will output an Abstract Syntax Tree of the program. 
* A **static analyzer** (in this case: a **type checker**): it will take as an input the Abstract Syntax Tree produced by the parser, and will report whether there are errors related to type mismatching (for example, adding `kg` to `km` should not be allowed, multiplying `kg` with `m/s` should not be allowed, etc.). Below you can find a table of what is considered "allowed" and "not allowed" in this particular language.
* An **interpreter** (in this case: an **evaluator**): it will take as an input the Abstract Syntax Tree produced by the parser, and will evaluate (i.e., calculate, that is, compute) the value of variables assigned by the assignment statement. Please note that all **mass units** should be converted to **grams**, all **length** units should be converted to **meters**, all **time units** should be converted to **seconds**, and all **velocity units** should be converted to **meters-per-second**. Please note that this document explains below how to convert various units, so you don't have to recall the physics course :)


Please note that as a part of the interpreter/evaluator, you'll have to deal with **environments** (a.k.a. "symbol tables") which store the names of variables and their values.

### What do we give you?

* A **tokenizer**: this is a part of a parser that _actually_ takes as an input the textual representation of a program written in _PhysicsLang_, and returns a list of tokens (lexemes). The parser then works with this list of tokens. This list is then processed by the parser that tries to identify a grammar rule which is applicable to parse this sequence of tokens.
* Various "technical details" of the solution, i.e., the skeleton of the solution. Please read the code comments for more details.

## Grammar for the language

To implement a parser, you will need to base off of a grammar for the _PhysicsLang_.
Here is the grammar for the language that you should base off of:

```js
// a program is a sequence of statements, i.e., a program can have any number of statements
Program :
    { Statement }*

// the only kind of statement we support in this language is the assignment statement
Statement :
    AssignmentStatement

// an assignment statement is an identifier followed by the equality symbol `=` followed by an expression
AssignmentStatement : 
    Identifier "=" Expr

// the definition of expression below takes into account priority of operations
// (namely, multiplication/division has a higher priority than addition/subtraction)
// an expression is an addition-like expression
Expr :
    Additive

// an addition-like expression is several multiplication-like expressions separated by an addition-like operator
// for example: 1 + 2*3 + 4 + 5*6 + 7*8
Additive :
    Multiplicative { OpAddSub Multiplicative }*

// an addition-like operator is either addition `+` or subtraction `-`
OpAddSub :
    "+" | "-"

// a multiplication-like expression is several primitive expressions separated by a multiplication-like operator
// for example: 1 * 2 * 3 * 4 * 5 * a * b * c * 6 * 7
Multiplicative :
    PrimitiveExpr { OpMulDiv PrimitiveExpr }*

// a multiplication-like operator is either multiplication `*` or division `/`
OpMulDiv :
    "*" | "/"

// a primitive expression is either:
PrimitiveExpr : 
    // an identifier (i.e., a use of a variable)
    Identifier
    // or a measured number
    | MeasuredNumber
    // or a group expression
    | GroupExpr

// a measured number is a number literal followed by a physical unit in square brackets
MeasuredNumber :
    Number "[" PhysicalUnit "]"

// a group expression is a an expression surrounded by parentheses 
GroupExpr :
    "(" Expr ")"

// a physical unit is either a mass unit, a length unit, a time unit, or a velocity unit
PhysicalUnit :
    Mass | Length | Time | Velocity

// a mass unit is either grams `g` or kilograms `kg`
Mass :
    "g" | "kg"

// a length unit is either meters `m` or kilometers `km`
Length :
    "m" | "km"

// a time unit is either seconds `s`, minutes `min`, or hours `h`
Time : 
    "s" | "min" | "h"

// a velocity unit is any combination of a length unit and a time unit, separated by a division symbol `/`
// i.e., it's one of these: `m/s`, `m/min`, `m/h`, `km/s`, `km/min`, `km/s`
Velocity :
    Length "/" Time
```

## What physical units are "matching"?

In the type checker, you'll need to implement functionality that would report an error if a program contains an expression where incompatible physical units are operands. For example, it should be **allowed** to add kilograms to grams, or to divide meters by hours, but it should **not be allowed** to multiply kilograms and kilometers.

"Compatible" physical units in this language _PhysicsLang_ are as specified as follows:

|operator|left operand|right operand|compatible?|remarks|
|---|---|---|---|---|
|`+`|`g` or `kg`|`g` or `kg`| :ok: yes| |
|`-`|`g` or `kg`|`g` or `kg`| :ok: yes| |
|`+`|`m` or `km`|`m` or `km`| :ok: yes| |
|`-`|`m` or `km`|`m` or `km`| :ok: yes| |
|`+`|`s` or `min` or `h`|`s` or `min` or `h`| :ok: yes| |
|`-`|`s` or `min` or `h`|`s` or `min` or `h`| :ok: yes| |
|`+`|`m/s` or `m/min` or `m/h` or `km/s` or `km/min` or `km/s`|`m/s` or `m/min` or `m/h` or `km/s` or `km/min` or `km/s`| :ok: yes| |
|`-`|`m/s` or `m/min` or `m/h` or `km/s` or `km/min` or `km/s`|`m/s` or `m/min` or `m/h` or `km/s` or `km/min` or `km/s`| :ok: yes| |
|`/`|`m` or `km`|`s` or `min` or `h`| :ok: yes| the right operand should _evaluate_ to non-zero; the result is a velocity |
|`*`|`m/s` or `m/min` or `m/h` or `km/s` or `km/min` or `km/s`|`s` or `min` or `h` | :ok: yes| the result is a length|
|`*`|`s` or `min` or `h`|`m/s` or `m/min` or `m/h` or `km/s` or `km/min` or `km/s`| :ok: yes| the result is a length|
|any operator|all other|all other|:x: no||

When implementing the type checker, it might be beneficial to have the following version of this table in mind:

|left operand|operator|right operand|result|remarks|
|---|---|---|---|---|
|mass|`+`|mass|mass| |
|mass|`-`|mass|mass| a negative mass is OK |
|length|`+`|length|length|  |
|length|`-`|length|length| a negative length is OK |
|time|`+`|time|time| |
|time|`-`|time|time| a negative time is OK |
|velocity|`+`|velocity|velocity| |
|velocity|`-`|velocity|velocity| a negative velocity is OK |
|length|`/`|time|**velocity**| the right operand should _evaluate_ to non-zero |
|velocity|`*`|time|**length**| |
|time|`*`|velocity|**length**| |
|all other|any operator|all other|:x: incompatible||


## Converting physical units when evaluating

When evaluating the values of expressions, you should convert all units to "basic" units as follows:

|unit|should be converted to|
|---|---|
|all **mass** units|grams (`g`)|
|all **length** units|meters (`m`)|
|all **time** units|seconds (`s`)|
|all **velocity** units|meters-per-second (`m/s`)|

Here is a more detailed table with conversion formulas:

|unit|should be converted to|formula|
|---|---|---|
|`g`|`g`|_already converted_|
|`kg`|`g`|multiply by `1000`|
|`m`|`m`|_already converted_|
|`km`|`m`|multiply by `1000`|
|`s`|`s`|_already converted_|
|`min`|`s`|multiply by `60`|
|`h`|`s`|multiply by `3600`|
|`m/s`|`m/s`|_already converted_|
|`m/min`|`m/s`|multiply by `0.01667`|
|`m/h`|`m/s`|multiply by `0.0002778`|
|`km/s`|`m/s`|multiply by `1000`|
|`km/min`|`m/s`|multiply by `16.667`|
|`km/h`|`m/s`|multiply by `0.2778`|

**Remarks:** 
- As a result of such conversions, the datatype of the evaluated values will be **double**.
- In TypeScript, instead of using the **double** type, one can use a more "general" type `number`. That's what we do in our skeleton solution.
- Please note that the _PhysicsLang_ language itself still only allows to explicitly use **integer**s.

## Example of a program and its Abstract Syntax Tree

Consider this program written in the _PhysicsLang_ language:

```js
let a = 10[kg] + 100[g];
```

### Here is a **tokenized version** of this program:

```json
[{"kind":"KeywordLet","value":"let"},{"kind":"Identifier","value":"a"},{"kind":"Equals","value":"="},{"kind":"Number","value":"10"},{"kind":"PhysicalUnit","value":"kg"},{"kind":"OpAddSub","value":"+"},{"kind":"Number","value":"100"},{"kind":"PhysicalUnit","value":"g"},{"kind":"Separator","value":";"}]
```

_Hint_: paste this code into https://codebeautify.org/jsonviewer to view it as a tree.

### Here is a **parsed version** (i.e., the AST) of this program:

```json
[{"assignee":{"name":"a","nodeType":"Identifier"},"expr":{"left":{"numericalValue":10,"unit":{"value":"kg","kind":1,"nodeType":"PhysicalUnit"},"nodeType":"MeasuredNumber"},"op":{"value":"+","nodeType":"OpAddSub"},"right":{"numericalValue":100,"unit":{"value":"g","kind":1,"nodeType":"PhysicalUnit"},"nodeType":"MeasuredNumber"},"nodeType":"Additive"},"nodeType":"AssignmentStatement"}]
```

_Hint_: paste this code into https://codebeautify.org/jsonviewer to view it as a tree.


## Skeleton of the skeleton of the solution

We give you quite a detailed skeleton of the solution.
Below is a **pseudocode** "**skeleton of a skeleton**", to give a _very_ high-level overview of what is going on.

### File `src/interpreter.ts`

```ts
function interpret(program: string): Environment {
  let tokenized = tokenizer.tokenize();
  let parsed = parseProgram(tokenized);
  typeCheck(parsed);
  evaluate(parsed);
  return environment;
}
```

### File `src/parser.ts`

```ts
function parseProgram(tokens: Token[]): AstNode[] {
  
  let currentPosition = -1;

  function getCurrentToken() { ... }
  function advance(): void { ... }
  function peek() { ... }
  function error() { ... }

  /*** functions for terminal symbols of the grammar ***/
  function KeywordLet(): void { ... }
  function Equals(): void { ... }
  function Semicolon(): void { ... }
  function OpeningBracket(): void { ... }
  function ClosingBracket(): void { ... }
  function NumericalLiteral(): number { ... }
  function Identifier(): Identifier { ... }
  function PhysicalUnit(): PhysicalUnit { ... }

  /*** functions for non-terminal symbols of the grammar ***/
  function Statement(): Statement { ... }
  function AssignStatement(): Assignment { ... }
  function Expr(): Expr { ... }
  function GroupExpr(): GroupExpr { ... }
  function PrimitiveExpr(): PrimitiveExpr { ... }
  function MeasuredNumber(): MeasuredNumber { ... }
  function OpAddSub(): Operator { ... }
  function OpMulDiv(): Operator { ... }
  function Additive(): ... { ... }
  function Multiplicative(): ... { ... }
}
```

### File `src/environment.ts`

```ts
interface Environment {
    // ...
}

function lookup(...) { ... }
function isDefined(...) { ... }
function declare(...) { ... }
function newEnv(...) { ... }
```

### File `src/evaluateVisitor.ts`

```ts
interface EvaluatedResult {
  ...
}

class EvaluateVisitor implements AbstractVisitor {

  constructor(...) { }

  visit(node: AstNode): EvaluatedResult {

    switch (...) {
      case "AssignmentStatement": ...
      case "Identifier": ...
      case "GroupExpr": ...
      case ...: ...
      ...
  }
}
```

### File `src/typeCheckVisitor.ts`

```ts
class TypeCheckVisitor implements AbstractVisitor {

  constructor(...) { }

  visit(node: AstNode): PhysicalUnitEnum {
    switch (...) {
      case "MeasuredNumber": ... 
      case "GroupExpr": ... 
      case "Identifier": ... 
      case ...: ...
      ...
   }
  }
}
```


## My progress 

- [ ] I have **carefully** read the spec of the project, i.e., this document.
- [ ] I do understand the idea of "tokenizer - parser - type checker - interpreter/evaluator".
- [ ] I have looked through the skeleton of the skeleton of the solution.
- [ ] I have looked through the repo with the skeleton of the solution: even if I don't necessarily understand everything exactly, I still understand roughly what each file there is _supposed_ to do.
- [ ] I do understand what an "evaluator" means and roughly how it works. (_Have I reviewed the slides and code from lectures about implementing evaluators?_)
- [ ] I do understand what a "type checker" means and roughly how it works. (_Have I reviewed the slides and code from the lectures about implementing a type checker?_)
- [ ] I do understand what an "environment" means and roughly how it works. (_Have I reviewed the slides and code from the lectures about environments?_)
- [ ] I do understand the example above of how an Abstract Syntax Tree looks like for the sample program.
- [ ] I'm fine with the JSON notation for the tokenized versions of programs and the JSON notation for ASTs (_See examples above in this document._).
- [ ] I have implemented all `TODO`s in the file `src/interpreter.ts`. :tada:
- [ ] I have implemented all `TODO`s in the file `src/environment.ts`. :tada:
- [ ] I have implemented all `TODO`s in the file `src/parser.ts`. :tada: :tada: :tada:
- [ ] I have implemented all `TODO`s in the file `src/evaluateVisitor.ts`. :tada: :tada:
- [ ] I have implemented all `TODO`s in the file `src/typeCheckVisitor.ts`. :tada: :tada:
- [ ] I've run my program on the given test examples, and everything works! :tada: :tada: :tada: :tada: :tada: :tada: :tada: :tada:
