# INF222 V24 - Obligatory Assignment 3 (deadline: 30.4.2024 at 23:59)

## Context
Consider the following Java code.

```java

// We have a class hierarchy with three classes: A, B, C
// (the classes are "empty", as their behaviour doesn't really matter for this task)
// (please keep them "empty", do not add any methods or fields etc.)
class A {}
class B extends A {}
class C extends B {}


// Here we have a class that represents a pair (i.e., a tuple of two elements).
class Pair<X,Y> {
    private X first;
    private Y second;

    // constructor
    Pair (X a, Y b) {
        this.first = a;
        this.second = b;
    }

    // a method to set the first element of the pair
    void setFirstElement(X a) {
        this.first = a;
    }

    // a method to get the second element of the pair
    Y getSecondElement() {
        return this.second;
    }

    // There are no other fields or methods in this class.
    // This means that:
    // it's forbidden to get the first element of the pair
    // it's forbidden to set the second element of the pair
}

// Here is how we will use this class.

// The `Main` class looks like this:
public class Main {

    // a method that demonstrates the use of the class `Pair` and the methods `setFirst` and `getSecond`
    public void getAndSetDemo(Pair<B,B> pair, B value) {
        pair.setFirstElement(value);
        B tmp = pair.getSecondElement();
    }
    
    // now we call the method `getAndSetDemo`
    public static void main(String[] args) {
        System.out.println("Hello Oblig3!");
        getAndSetDemo(new Pair<A,C>(new A(), new C()), new B());
        getAndSetDemo(new Pair<C,A>(new C(), new A()), new B());
    }
}
```

Unfortunately, this code doesn't compile.
- The reason is that class `Pair` is invariant.
- Java doesn't support declaration-site variance, so it's impossible to specify covariance/contravariance for the generic parameters `X` and `Y` of class `Pair`.
- However, Kotlin **does support declaration-site variance** (using `MyClass<out T>` to designate that `MyClass` is covariant in `T`, and using `MyClass<in T>` to designate that `MyClass` is contravariant in `T`).

## Your task

**Your task is to manually translate the Java code into Kotlin and specify variance for generic parameters `X` and `Y`.**

**You can use [REPL.IT](https://repl.it/) to write and run Kotlin code.**

Please note that the class `Pair` is contravariant in `X` and covariant in `Y`.
Please also note that `C` is a **sub**type of `B`, and `B` is a **sub**type of `A` (in other words, `A` is a **super**type of `B`).
Thus, `Pair<A,C>` **is a subtype** of `Pair<B,B>` (because `A` is a SUPERtype of `B` - _CONTRAvariance_, and `C` is a SUBtype of `B` - _COvariance_), but `Pair<C,A>` is **not** a subtype of `Pair<B,B>`.

In terms of the code above:
```java
        ...
        getAndSetDemo(new Pair<A,C>(new A(), new C()), new B()); // when translated to Kotlin, this line should work, because `Pair<A,C>` is a subtype of `Pair<B,B>`
        getAndSetDemo(new Pair<C,A>(new C(), new A()), new B()); // when translated to Kotlin, an error will be reported about this line, because `Pair<C,A> is not a subtype of `Pair<B,B>` (so, just comment this line in your Kotlin translation)
        ...
```

_FAQ:_

_Q: Why do we care whether something is a subtype of `Pair<B,B>`?_

_A: Because we call method `getAndSetDemo(..., ...)`, where, according to the Liskov subsitution principle, the first argument is supposed to be a subtype of `Pair<B,B>`. Indeed, `Pair<B,B>` is the type of the first formal parameter of that method: `public void getAndSetDemo(Pair<B,B> ......)`._


### Hints :sparkles:
- **see slides 85-86 of https://mitt.uib.no/courses/45696/files/folder/lecture-slides?preview=5817459**
- **Kotlin documentation on class constructors: https://kotlinlang.org/docs/classes.html#constructors (pay special attention to `var` and `val`)**
- **see slides 74-75 of https://mitt.uib.no/courses/45696/files/folder/lecture-slides?preview=5817459**
- when translating from Java to Kotlin, it's important to keep the names of fields `first` and `second`, and methods `setFirstElement` and `getSecondElement` (i.e., **use these exact names for the fields and methods in your Kotlin code**)

### Extra reading :books:
- Kotlin documentation on declaration-site variance: https://kotlinlang.org/docs/generics.html#declaration-site-variance
- a tutorial on variance in Kotlin: https://typealias.com/concepts/declaration-site-variance/

### Warning :exclamation:
- **Do not use[`@UnsafeVariance` decorator](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unsafe-variance/).**



## Skeleton of the solution

Here is a skeleton of the solution in Kotlin:

```kotlin
open class A {}
open class B : A() {}
open class C : B() {}


class Pair......................... {

  fun setFirstElement.................

  fun getSecondElement................

}


// getAndSetDemo
............


fun main() {
  println("Hello Oblig3!")
  ........................
}
```

## ChatGPT 3.5 :white_check_mark:

**For this particular Obligatory Assignment, you are allowed to use ChatGPT 3.5.**

For your convenience, here is a lengthy discussion with ChatGPT about this task (the task formulation is copy-pasted exactly from the Oblig's text):
https://chat.openai.com/share/e414b40b-50aa-4121-9f23-ef9274dac7cb

And here is another discussion with ChatGPT:
https://chat.openai.com/share/710fb768-da7a-4e0f-95fd-f4b2343f2a44
(stopped after ChatGPT has generated the same reply as in the other discussion linked above).

As you can see, ChatGPT 3.5 just doesn't seem to work for this task.
If you manage ChatGPT 3.5 to solve this task, please include in your Oblig3 submission the link to a shared chat.

## ChatGPT 4.0 :x:

**ChatGPT 4.0 solves this task immediately!**

**Be fair to yourself and to the other students of INF222, do not use ChatGPT 4.0.**

_FAQ:_

Q: I couldn't hold myself and I still used ChatGPT 4.0. I have seen the solution and can't "unsee" it now. I'm really sorry about this. What do I do now?

A: For us, there is no point in grading a solution generated by ChatGPT 4.0, it'll be almost perfect (ChatGPT 4.0 still does generate code that is a bit off of the task description, but the code works perfectly). Obligatory Assignments in this course are not there to test you -- they are for you to learn new stuff. To make it fair and for you to actually learn, you will be asked to **make slides and a 10 minute video presentation where you explain in detail this task and the solution to it.** It's OK to just have in the recording your screen with the slides and the voice-over. If you don't feel comfortable with your voice being recorded, then you should have captions appearing at moments where you'd pronounce that piece of text.
